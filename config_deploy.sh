#!/bin/sh
echo PATH_PROJECT=$PATH_PROJECT   >> .env
echo DOCKER_REGISTRY=$DOCKER_REGISTRY   >> .env
echo VERSION_BACKEND=$(curl --request GET "https://gitlab.com/api/v4/projects/$ID_GITLAB_PROJECT_BACKEND/repository/tags" | jq -r '.[0] | .name')>> .env
echo VERSION_FRONTEND=$(curl --request GET "https://gitlab.com/api/v4/projects/$ID_GITLAB_PROJECT_FRONTEND/repository/tags" | jq -r '.[0] | .name')>> .env
echo APP_NAME_BACKEND=$APP_NAME_BACKEND   >> .env
echo APP_NAME_FRONTEND=$APP_NAME_FRONTEND   >> .env
echo PORT_BACKEND=$PORT_BACKEND >> .env
echo PORT_FRONTEND=$PORT_FRONTEND >> .env
echo LOKI_SERVER=$LOKI_SERVER >> .env