ssh -o StrictHostKeyChecking=no $EC_USER@$EC2_PUBLIC_IP_ADDRESS << 'ENDSSH'
  cd /home/ubuntu/felipe/profile
  export $(cat .env | xargs)
  aws ecr get-login-password | docker login --username AWS --password-stdin $DOCKER_REGISTRY
  docker pull $DOCKER_REGISTRY/$APP_NAME_FRONTEND:$VERSION_FRONTEND
  docker pull $DOCKER_REGISTRY/$APP_NAME_BACKEND:$VERSION_BACKEND
  docker compose down
  docker compose up -d
  docker image prune -a -f
ENDSSH